module.exports = {
  launchOptions: {
    headless: true
  },
  browser: ["chromium"],
  exitOnPageError: false,
  devices: [],
  setupFilesAfterEnv: ['jest-playwright-allure/src/registerAllureReporter'],
  reporters: ["default", "jest-playwright-allure"]
}
