module.exports = {
    verbose: true,
    preset: 'jest-playwright-preset',
    testMatch: [
      "**/__tests__/**/*,+(js)", "**/?(*.)+(spec|test).+(js)"
    ],
    testRunner: "jest-jasmine2",
    testTimeout: 20000,
    setupFilesAfterEnv: ['jest-playwright-allure/src/registerAllureReporter'],
    reporters: ["default", "jest-playwright-allure"]
};
